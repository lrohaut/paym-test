# paym-test

**1.1 Describe the command you use to connect to this cluster.**

--> Save the kubeconfig file at "$kubeconfig_file"

As there is only one context and one user :

using kubectl : kubectl --kubeconfig=$kubeconfig_file

Example to list nodes : kubectl get nodes --kubeconfig=$kubeconfig_file

**1.2 Give a detailed output of the cluster node(s) (kubernetes version, IP, OS Image, Container runtime)**

--> Using the command: "kubectl get nodes --kubeconfig=kubeconfig.txt -o wide", we get a more detailed output (-o json is more detailed, but for this question wide is enough)

The output is :

```
NAME                                             STATUS   ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP     OS-IMAGE                        KERNEL-VERSION     CONTAINER-RUNTIME
scw-devops-test-devops-test-pool-b48836-443dd2   Ready    <none>   3h22m   v1.20.2   10.68.0.81    51.15.130.186   Ubuntu 20.04.1 LTS 53147b6f27   5.4.0-53-generic   containerd://1.4.3
```

**2 - Setup a load balancer to expose internal services**

**2.1 Deploy a modified Nginx controller for Scaleway Provider**

--> Download the deployment corresponding to the scaleway provider:

wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/scw/deploy.yaml


```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/scw/deploy.yaml
```


Add to the service the annotation : "service.beta.kubernetes.io/scw-loadbalancer-use-hostname: 'true'",which will force the use of a hostname instead of a public IP

Deploy the modified file.

```
$ kubectl --kubeconfig=../kubeconfig.txt apply -f deploy.yaml
```


**Ckeck that the load balancer is correctly deployed as a hostname (write in your README the kubectl command to check this information and its result).**

We can see that the resources are created in the ingress-nginx namespace:

```
$ kubectl --kubeconfig=../kubeconfig.txt get service -n ingress-nginx
NAME                                 TYPE           CLUSTER-IP      EXTERNAL-IP                        PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.44.221.161   51-159-24-49.lb.fr-par.scw.cloud   80:30687/TCP,443:31681/TCP   2m15s
ingress-nginx-controller-admission   ClusterIP      10.43.240.190   <none>                             443/TCP                      2m16s
```

The hostname is : "51-159-24-49.lb.fr-par.scw.cloud"

**2.3 Could you explain our choice to prefer a hostname instead of an IP for our load balancer ?**

--> The IP can change, the hostname can stay the same.

**3 - RabbitMQ deployment**

**3.1 Deploy version 1.3.0 of the RabbitMQ cluster-operator**

--> As it is not the latest version we have to change a bit the command provided by the documentation:

```
kubectl --kubeconfig=../kubeconfig.txt apply -f https://github.com/rabbitmq/cluster-operator/releases/download/v1.3.0/cluster-operator.yml
```

**3.2 Deploy a RabbitMQ cluster**

**Replicas: Without applying it, explain how you would scale the number of replicas to 3.**

--> To deploy multiple replicas, we simply have to add the following to our RabbitmqCluster resource:

```
spec:
	replicas: 3
```

And then use kubectl apply or patch.

**3.3.1 Give the command to connect locally on the remote dashboard**

--> We have to use port-forwarding:

```
$ kubectl --kubeconfig=../kubeconfig.txt port-forward "service/simple-rabbit" 15672
```

And then access it from the web navigator at "localhost:15672"

**3.3.2 Retrieve the credentials and connect as an admin on Rabbitmq dashboard**

**Explain how to retrieve these credentials.**

--> The credentials are available as kubernetes secrets. To retrieve them:

```
$ kubectl --kubeconfig=../kubeconfig.txt get secret simple-rabbit-default-user -o jsonpath='{.data.username}' | base64 --decode
***
$ kubectl --kubeconfig=../kubeconfig.txt get secret simple-rabbit-default-user -o jsonpath='{.data.password}' | base64 --decode
***
```

**Connect to the dashboard and create a new user named paymium (Admin -> Add User).**

--> Done !

**3.4 Expose the rabbitmq cluster on Internet**

**Create an ingress file to expose the rabbitmq management service on Internet.**

--> The file is in this repository as "rabbit_ingress.yml":

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
	name: ingress-rabbitmq
	annotations:
	kubernetes.io/ingress.class: "nginx"
spec:
	rules:
	- host: 51-159-24-49.lb.fr-par.scw.cloud
	http:
		paths:
		- path: /
		backend:
			serviceName: simple-rabbit
			servicePort: 15672
```

**Deploy this ingress file and check that this service is available online.**

--> Yes, all good.

**3.5 Explain how to secure the connection**

Using Let's Encrypt certificates and if you would have access to paymium.com domain, explain how you would do to have:

Secure acess to rabbitmq management using https://simple-rabbit.paymium.com

--> We can use the Certmanager with the let's encrypt issuer:
	
We will need to deploy the cert manager first, using for example the command "$ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.1.0/cert-manager.yaml"

We will have to add an annotation to our ingress: cert-manager.io/issuer: "letsencrypt-paymium"

We will have to change the rule for our ingress with "host: simple-rabbit.paymium.com"

Then create an Issuer for let's encrypt:
	
```
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
	name: letsencrypt-paymium
spec:
	paymium:
	server: https://simple-rabbit.paymium.com
	email: amail@mailme.com
	privateKeySecretRef:
		name: letsencrypt-simple-rabbit
	solvers:
	- http01:
		ingress:
			class:  nginx	
```
	
And finally we will have to add tls to our ingress resource, so the final ingress would be:
	
```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
	name: ingress-rabbitmq
	annotations:
	kubernetes.io/ingress.class: "nginx"
	ingress: cert-manager.io/issuer: "letsencrypt-paymium"
spec:
	tls:
	- hosts:
	- simple-rabbit.paymium.com
	secretName: nomDuSecretFournisParCertManager
	rules:
	- host: simple-rabbit.paymium.com
	http:
		paths:
		- path: /
		backend:
			serviceName: simple-rabbit
			servicePort: 15672
```
	

